import Vue from 'vue';
import App from './App';
import router from './router';
import jQuery from 'jquery';
window.jQuery = jQuery;
require('bootstrap/js/affix');
require('bootstrap/js/alert');
require('bootstrap/js/button');
require('bootstrap/js/carousel');
require('bootstrap/js/collapse');
require('bootstrap/js/dropdown');
require('bootstrap/js/modal');
require('bootstrap/js/scrollspy');
require('bootstrap/js/tab');
require('bootstrap/js/tooltip');
require('bootstrap/js/popover');
require('bootstrap/js/transition');
import Scroller from './components/Scroller';
Vue.component('scroller', Scroller);


/* eslint-disable no-new */
require(['./content/site-config.json'], (data) => {
    window.siteConfig = data.fields;
    new Vue({
        el: '#app',
        router,
        data: {title: 'hello test'},
        render: (h) => h(App),
    });
});

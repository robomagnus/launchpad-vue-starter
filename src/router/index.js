import Vue from 'vue';
import Router from 'vue-router';
import Home from 'components/Home';
import Page from 'components/Page';
import * as siteConfig from '../content/site-config.json';

Vue.use(Router);

siteConfig.fields.navigation.map((nav) => {
    nav.path = '/page/:id';
    // nav.name = nav.title;
    nav.component = Page;
});
siteConfig.fields.navigation.push({path: '/', name: 'Home', component: Home});

console.log(siteConfig.fields.navigation);

export default new Router({
    routes: siteConfig.fields.navigation,
});

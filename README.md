# Magnus Launchpad

> Magnus Design Project Starter

This starter project is designed for use with contentful services.
It is based on VueJS cli webpack template.

## Quick Start
```
npm install
// bootstrap content for dev if configured
node build/bootstrap-content.js
npm run dev
```

## Config
### Example Content Config
```
module.exports = {
    space: <space-id>,
    accessToken: <access-token>
}
```

### Bootstrap Content for Dev
```
node build/bootstrap-content.js
```

## Package Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

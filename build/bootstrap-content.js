var contentful = require('contentful');
var fs = require('fs');
var _ = require('lodash');
var chalk = require('chalk');
var config = require('../config');

var client = contentful.createClient({
    space: process.env.NODE_ENV === 'production'
        ? config.build.env.content.space
        : config.dev.env.content.space,
    accessToken: process.env.NODE_ENV === 'production'
        ? config.build.env.content.accessToken
        : config.dev.env.content.accessToken
});

var CONTENT_DIR = 'src/content/';
var STATIC_DIR = 'static/';

function saveToFile(contentObj, filename) {

    if (typeof contentObj === 'object') {
        fs.writeFile(`${CONTENT_DIR}${filename}.json`, JSON.stringify(contentObj, null, 4), function(err) {
            if(err) {
                return console.log(err);
            }
            console.log(`The file ${filename} was saved!`);
        });
        fs.writeFile(`${STATIC_DIR}${filename}.json`, JSON.stringify(contentObj, null, 4), function(err) {
            if(err) {
                return console.log(err);
            }
            console.log(`The file ${filename} was saved!`);
        });
    }
}

console.log(chalk.bold.blue('Getting entries...'));
client.getEntries({
        // 'sys.contentType.sys.id': 'page'
        // order: 'fields.order'
    })
    .then((entries) => {
        console.log(chalk.bold.blue('Entries ------>'), entries);
        var homePage = [];
        var pages = [];
        var siteConfig = [];

        entries.items.forEach((entry) => {
            if (entry.sys.contentType.sys.id === 'page') {
                pages.push(entry);
            } else if (entry.sys.contentType.sys.id === 'siteConfig') {
                siteConfig.push(entry);
            } else if (entry.sys.contentType.sys.id === 'homePage') {
                homePage.push(entry);
            }
        });

        if (siteConfig.length > 0 && siteConfig[0].fields) {
            siteConfig = siteConfig[0];
            siteConfig.fields.navigation = [];
        } else {
            throw new Error('site configuration not found');
        }

        if (homePage.length > 0) {
            saveToFile(homePage[0], 'home-page');
        }

        console.log('\n\nPages ----->\n', pages, '\n\n<----');

        pages.forEach(page => {
            siteConfig.fields.navigation.push({
                title: page.fields.title,
                slug: page.fields.slug
            });
            saveToFile(page, page.fields.slug);
        });

        console.log('\n\nSite Config ---->\n', siteConfig, '\n\n<-----')

        saveToFile(siteConfig, 'site-config');
    })
    .catch(err => console.log(err));


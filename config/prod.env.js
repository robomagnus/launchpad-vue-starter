var contentConfig = require('./secure/content-config');

module.exports = {
    NODE_ENV: '"production"',
    content: contentConfig
}
